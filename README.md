# Recovida

Este repositório e os demais repositórios deste grupo fazem parte do projeto
**Recovida**
(*Reavaliação da Mortalidade por Causas Naturais no Município de São Paulo
durante a Pandemia da COVID-19*),
da
[Faculdade de Medicina da Universidade de São Paulo](https://www.fm.usp.br/),
sob responsabilidade do
[Prof. Dr. Paulo Andrade Lotufo](https://uspdigital.usp.br/especialistas/especialistaObter?codpub=F7A214F0B89F),
e com a atuação da [Dra. Ana Carolina de Moraes Fontes Varella](https://bv.fapesp.br/en/pesquisador/690479/ana-carolina-de-moraes-fontes-varella/) como supervisora de dados.



## Apoio

Agradecemos à iniciativa [Todos pela Saúde](https://www.todospelasaude.org/),
da [Fundação Itaú para Educação e Cultura](https://fundacaoitau.org.br/),
pelo financiamento deste projeto. 

Agradecemos também à
[Secretaria Municipal da Saúde da Prefeitura da Cidade de São Paulo](https://www.prefeitura.sp.gov.br/cidade/secretarias/saude/)
pela parceria durante a execução do projeto. 



## Autores

Sob orientação de Paulo Lotufo e a supervisão de Ana Varella,
os programas estão sendo desenvolvidos pelos seguintes membros do projeto:

- Débora Lina Nascimento Ciriaco Pereira (bolsista de dez/2020 a set/2021);
- Vinícius Bitencourt Matos (bolsista de dez/2020 a set/2021).


## Lista de ferramentas

#### 1. [Augmented SIM](https://gitlab.com/recovida/augmented-sim):

A partir de tabelas com dados de óbitos codificados de acordo com o
*Sistema de Informação sobre Mortalidade* (SIM), o programa adiciona
colunas com informações obtidas a partir das colunas já existentes
e gera um único arquivo com todos os dados.

#### 2. [IDAS-RL](https://gitlab.com/recovida/idas-rl) \([linha de comando](https://gitlab.com/recovida/idas-rl-core) e [interface gráfica - LinkaSUS](https://gitlab.com/recovida/idas-rl-gui)\):

A partir de duas bases de dados A e B, o programa encontra pares de linhas (uma de cada base)
que provavelmente correspondem a um mesmo indivíduo, por meio de algoritmos de indexação
e *scoring*.

